SHOW DATABASES;

-- Create a database
CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users (
	-- columns --
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts (
	-- columns --
	id INT NOT NULL AUTO_INCREMENT,
	author_id INT NOT NULL,
	title VARCHAR(500) NOT NULL,
	content VARCHAR (5000),
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (author_id) REFERENCES users(id)
);

CREATE TABLE posts_comments (
	id INT NOT NULL AUTO_INCREMENT,
	posts_id INT NOT NULL,
	users_id INT NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_comments_posts_id
		FOREIGN KEY (posts_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_comments_users_id
		FOREIGN KEY (users_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT

);

CREATE TABLE posts_likes (
	id INT NOT NULL AUTO_INCREMENT,
	posts_id INT NOT NULL,
	users_id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_likes_posts_id
		FOREIGN KEY (posts_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_likes_users_id
		FOREIGN KEY (users_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT

);
